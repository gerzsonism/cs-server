// Dependencies
const spawn = require('child_process').spawn;
const robot = require('robotjs');
const ncp   = require('copy-paste');

// Process variables
const mode = process.env.GAME_MODE;
const type = process.env.GAME_TYPE;
const map = process.env.GAME_MAP;

// Computer properties
const screenSize = robot.getScreenSize();
const path = 'C:\\Program Files (x86)\\Steam\\steamapps\\common\\Counter-Strike Global Offensive\\csgo.exe';

// Game params
const consoleShortcut = 'f2';
const optionalParams = [
    'mp_friendlyfire 1',
    'mp_solid_teammates 2',
    'mp_freezetime 5',
    'mp_autoteambalance 0'
];

let gameStartedBootup = false;
let gameMode;

switch (process.env.gameMode) {
    case '1': gameMode = 'Casual';      break;
    case '2': gameMode = 'Competitive'; break;
    case '3': gameMode = 'Arms Race';   break;
    case '4': gameMode = 'Deathmatch';  break;
    case '5': gameMode = 'Demolition';  break;
    case '6': gameMode = 'Wingman';     break;
}

// Starting server
const cs = spawn(path, ['-steam']);

console.log('\x1b[97m', `Starting ${map.includes('/') ? map.split('/')[2] : map} as ${gameMode}`);

if (optionalParams.length) {
    ncp.copy(optionalParams.join(';'));

    console.log('\x1b[97m', 'Using additional params:\n');
    console.log('\x1b[37m', `- ${optionalParams.join('\n - ')}`);
    console.log('\x1b[92m', '\n copied additional params to clipboard\n');
}

function checkGameState() {
    const mouse = getSingleScreenMousePosition(robot.getMousePos());
    const hex = robot.getPixelColor(mouse.x, mouse.y);

    robot.moveMouse(0, screenSize.height / 2);

    const onLoadingScreen = hex === '000000' && mouse.x === 0;
    const onMainMenu      = hex !== '000000' && mouse.x === 0;

    if (!gameStartedBootup && onLoadingScreen) {
        console.log('\x1b[37m', 'Game booting up');
        gameStartedBootup = true;
    }

    if (gameStartedBootup && onMainMenu) {
        console.log('\x1b[92m', 'Game loaded, starting server...');

        robot.setKeyboardDelay(500);
        robot.keyTap(consoleShortcut);

        robot.typeString(`game_mode ${mode}`);
        robot.keyTap('enter');

        robot.typeString(`game_type ${type}`);
        robot.keyTap('enter');

        robot.typeString(`map ${map}`);
        robot.keyTap('enter');

        clearInterval(gameStateInterval);

        process.kill(process.env.PID);
    }
}

function getSingleScreenMousePosition(mouse) {
    let x = mouse.x;
    let y = mouse.y;

    if (mouse.x < 0 || mouse.x > screenSize.width) {
        x = 0;
    }

    if (mouse.y < 0 || mouse.y > screenSize.height) {
        y = 0;
    }

    return {
        x,
        y
    };
}

const gameStateInterval = setInterval(checkGameState, 1000);
