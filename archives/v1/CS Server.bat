@echo off

:: Set title of the window
title CS Server

:: Get the process id
for /f "usebackq tokens=2" %%i in (`tasklist /fi "windowtitle eq CS Server" /fo list ^| find /i "PID:"`) do (
    set PID=%%i
)

:: 🎮 Select game mode
:selectGameMode
    echo.
    echo. Select game mode:
    echo. =================
    echo.

    echo.  1. Casual
    echo.  2. Competitive
    echo.  3. Arms Race
    echo.  4. Deathmatch
    echo.  5. Demolition
    echo.  6. Wingman

    echo.

    set /p GameMode=

    if %GameMode% LSS 1 goto invalidGameMode
    if %GameMode% GTR 6 goto invalidGameMode

    :: Casual
    if %GameMode%==1 (
        set GAME_MODE=0
        set GAME_TYPE=0
    )

    :: Competitive
    if %GameMode%==2 (
        set GAME_MODE=1
        set GAME_TYPE=0
    )

    :: Arms Race
    if %GameMode%==3 (
        set GAME_MODE=0
        set GAME_TYPE=1
    )

    :: Deatchmatch
    if %GameMode%==4 (
        set GAME_MODE=2
        set GAME_TYPE=1
    )

    :: Demolition
    if %GameMode%==5 (
        set GAME_MODE=1
        set GAME_TYPE=1
    )

    :: Wingman
    if %GameMode%==6 (
        set GAME_MODE=2
        set GAME_TYPE=0

        cls
        goto selectWingmanMap
    )

    cls
    goto selectMap

:: 🗺️ Select map
:selectMap
    echo.
    echo. Select map:
    echo. ===========
    echo.

    :: ar maps
    echo.  1. ar_baggage
    echo.  2. ar_dizzy
    echo.  3. ar_monastery
    echo.  4. ar_shoots

    :: cs maps
    echo.  5. cs_agency
    echo.  6. cs_assault
    echo.  7. cs_italy
    echo.  8. cs_militia
    echo.  9. cs_office

    :: de maps
    echo.  10. de_austria
    echo.  11. de_bank
    echo.  12. de_biome
    echo.  13. de_cache
    echo.  14. de_canals
    echo.  15. de_cbble
    echo.  16. de_dust2
    echo.  17. de_inferno
    echo.  18. de_lake
    echo.  19. de_mirage
    echo.  20. de_nuke
    echo.  21. de_overpass
    echo.  22. de_safehouse
    echo.  23. de_shortdust
    echo.  24. de_shortnuke
    echo.  25. de_stmarc
    echo.  26. de_subzero
    echo.  27. de_sugarcane
    echo.  28. de_train

    :: workshop maps
    echo.  29. abbottabad
    echo.  30. breach
    echo.  31. kabul
    echo.  32. kaimas
    echo.  33. museum
    echo.  34. no mercy
    echo.  35. pool day
    echo.  36. renovation
    echo.  37. sauna
    echo.  38. siege

    set /p GameMap=

    if %GameMap% LSS 1 goto invalidMap
    if %GameMap% GTR 38 goto invalidMap

    :: ar maps
    if %GameMap%==1 set GAME_MAP=ar_baggage
    if %GameMap%==2 set GAME_MAP=ar_dizzy
    if %GameMap%==3 set GAME_MAP=ar_monastery
    if %GameMap%==4 set GAME_MAP=ar_shoots

    :: cs maps
    if %GameMap%==5 set GAME_MAP=cs_agency
    if %GameMap%==6 set GAME_MAP=cs_assault
    if %GameMap%==7 set GAME_MAP=cs_italy
    if %GameMap%==8 set GAME_MAP=cs_militia
    if %GameMap%==9 set GAME_MAP=cs_office

    :: de maps
    if %GameMap%==10 set GAME_MAP=de_austria
    if %GameMap%==11 set GAME_MAP=de_bank
    if %GameMap%==12 set GAME_MAP=de_biome
    if %GameMap%==13 set GAME_MAP=de_cache
    if %GameMap%==14 set GAME_MAP=de_canals
    if %GameMap%==15 set GAME_MAP=de_cbble
    if %GameMap%==16 set GAME_MAP=de_dust2
    if %GameMap%==17 set GAME_MAP=de_inferno
    if %GameMap%==18 set GAME_MAP=de_lake
    if %GameMap%==19 set GAME_MAP=de_mirage
    if %GameMap%==20 set GAME_MAP=de_nuke
    if %GameMap%==21 set GAME_MAP=de_overpass
    if %GameMap%==22 set GAME_MAP=de_safehouse
    if %GameMap%==23 set GAME_MAP=de_shortdust
    if %GameMap%==24 set GAME_MAP=de_shortnuke
    if %GameMap%==25 set GAME_MAP=de_stmarc
    if %GameMap%==26 set GAME_MAP=de_subzero
    if %GameMap%==27 set GAME_MAP=de_sugarcane
    if %GameMap%==28 set GAME_MAP=de_train

    :: workshop maps
    if %GameMap%==29 set GAME_MAP=workshop/140660512/fy_abbottabad_go
    if %GameMap%==30 set GAME_MAP=workshop/1258599704/de_breach
    if %GameMap%==31 set GAME_MAP=workshop/232135055/de_kabul_b3
    if %GameMap%==32 set GAME_MAP=workshop/1428210719/fy_kaimas
    if %GameMap%==33 set GAME_MAP=workshop/127012360/cs_museum
    if %GameMap%==34 set GAME_MAP=workshop/145957858/de_no_mercy_b2
    if %GameMap%==35 set GAME_MAP=workshop/122521875/fy_pool_day
    if %GameMap%==36 set GAME_MAP=workshop/1477762277/cs_renovation
    if %GameMap%==37 set GAME_MAP=workshop/128685543/cs_sauna_go
    if %GameMap%==38 set GAME_MAP=workshop/888788311/cs_housesiege

    cls
    goto launchGame

:: 👫 Select wingman map
:selectWingmanMap
    echo.
    echo. Select map:
    echo. ===========
    echo.

    :: ar maps
    echo.  1. de_cbble
    echo.  2. de_inferno
    echo.  3. de_lake
    echo.  4. de_overpass
    echo.  5. gd_rialto
    echo.  6. de_shortdust
    echo.  7. de_shortnuke
    echo.  8. de_train
    echo.  9. de_vertigo

    set /p GameMap=

    if %GameMap% LSS 1 goto invalidWingmanMap
    if %GameMap% GTR 9 goto invalidWingmanMap

    :: maps supporting wingman mode
    if %GameMap%==1 set GAME_MAP=de_cbble
    if %GameMap%==2 set GAME_MAP=de_inferno
    if %GameMap%==3 set GAME_MAP=de_lake
    if %GameMap%==4 set GAME_MAP=de_overpass
    if %GameMap%==5 set GAME_MAP=gd_rialto
    if %GameMap%==6 set GAME_MAP=de_shortdust
    if %GameMap%==7 set GAME_MAP=de_shortnuke
    if %GameMap%==8 set GAME_MAP=de_train
    if %GameMap%==9 set GAME_MAP=de_vertigo

    cls
    goto launchGame

:: 🙌 Launching game
:launchGame
    node CSScript.js

    pause

:: Check for invalid game mode
:invalidGameMode
    echo. Invalid game mode, input must be between 1-5
    goto selectGameMode

:: ...And for invalid map
:invalidMap
    echo. Invalid map, input must be between 1-38
    goto selectMap

:: ...And for invalid map on wingman mode
:invalidWingmanMap
    echo. Invalid map, input must be between 1-9
    goto selectWingmanMap
