## Prerequirements
- `npm install` (in administrator mode)

## Steps to run
- Run `CS Server.bat`
- Choose a game mode and a map by typing in the number and hitting enter
- `CS Server` will take over the mouse for the time the game loads, do not move
- Press shift after map is loaded in, it is toggled on for some reason

### Notes
- Make sure Steam runs
- Enable console in CS Go
- Console shortcut must be on F2, alternatively you can change the `CS_CONSOLE_SHORTCUT` variable inside `config.js`
- If the game fails to run, check the `CS_ROOT_PATH` variable inside `config.js` on line 13
- Add optional params set inside `config.js`, if needed, they will be copied to the clipboard for you to paste into the cli after the match starts
