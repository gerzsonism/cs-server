const selectedMenuItem = {
    gameMode: {
        name: 'Competitive',
        type: 0,
        mode: 0
    },
    map: {
        mapName: '',
        mapPath: ''
    }
};

const menuOptions = [
    'SZEGGYEDALÁ',
    'Game Mode',
    'Classic maps',
    'Workshop maps',
    'Exit'
];

module.exports = {
    selectedMenuItem,
    menuOptions
};
